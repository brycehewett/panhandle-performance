<?php get_header(); ?>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php if ( has_post_thumbnail() ) { ?>
    <div class="page-feature-image" style="background: url(<?php the_post_thumbnail_url( 'large' ); ?>) center no-repeat; background-size: cover;">

      <div class="container feature-overlay-text">
        <h1><?php the_title(); ?></h1>
      </div>

    </div>
  <?php } ?>
    <div class="breadcrumbs">
      <div class="container">
        <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>
      </div>
    </div>

    <div class="container content">
      <article class="post" id="post-<?php the_ID(); ?>">

        <?php if ( !has_post_thumbnail() ) { ?>
          <h1><?php the_title(); ?></h1>
        <?php } ?>

        <div class="entry">
            <?php the_content(); ?>
        </div>
      </article>

    <?php endwhile; endif; ?>
  </div>

<?php get_footer(); ?>
