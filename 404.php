<?php get_header(); ?>

    <div class="container content">
    	<div class="two-third-column" id="main-column">
	    	<article>
				<h1><?php _e('Error 404 - Page Not Found','html5reset'); ?></h1>

				<p>Sorry, we couldn't find the page you were looking for.<br />Try using the search box below to find it.</p>

				<?php get_search_form(); ?>

				<?php if (function_exists('wbz404_suggestions')) { wbz404_suggestions(); } ?>
			</article>
		</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>
