 <?php
/*
Template Name: Contact
*/

?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="page-feature-image">
      <div id="map_canvas"></div>
    </div>
    <div class="breadcrumbs">
      <div class="container">
        <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>
      </div>
    </div>

    <div class="container content">
      <div class="two-third-column">
        <article class="post" id="post-<?php the_ID(); ?>">

          <h1><?php the_title(); ?></h1>

          <div class="entry">
            <?php the_content(); ?>
          </div>

          <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

        </article>

      <?php endwhile; endif; ?>
      </div>
      <?php get_sidebar(); ?>
      <script>
        function initMap() {
          var latLng = new google.maps.LatLng(30.226173,-85.6503);
          var options = {
            zoom: 17,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false,
            scrollwheel: false,
          };

          var map = new google.maps.Map(document.getElementById("map_canvas"), options);

          var image = '/wp-content/themes/panhandle-performance.git/images/pin.png';
          var marker = new google.maps.Marker({
              position: latLng,
              map: map,
              icon: image
          });

        }

        google.maps.event.addDomListener(window, 'load', initMap);
      </script>
    </div>

<?php get_footer(); ?>
