 <?php
/*
Template Name: Sidebar
*/
?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php if ( has_post_thumbnail() ) { ?>
    <div class="primary-promo">

        <div class="container">
          <div id="promo-text">
            <h1><?php the_title(); ?></h1>
          </div>
        </div>
        
        <?php the_post_thumbnail('large'); ?>
       
    </div>
  <?php } ?>

    <div class="container content">
      <div id="main-column">
      <article class="post" id="post-<?php the_ID(); ?>">

        <?php if ( !has_post_thumbnail() ) { ?>
          <h1><?php the_title(); ?></h1>
        <?php } ?>
        
        <div class="entry">

          <?php the_content(); ?>

        </div>

        <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

      </article>
    
    <?php endwhile; endif; ?>
  </div>
  <?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
