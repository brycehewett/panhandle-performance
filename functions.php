<?php

	// Add RSS links to <head> section
	automatic_feed_links();

  function load_dependencies() {
      //Styles
			wp_enqueue_style( 'material-design-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
			wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );

			//Scripts in the header
      wp_register_script('jQuery', ('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'),'1.11.1', false);

			//Scripts in the footer
			wp_register_script('map-api', ('https://maps.googleapis.com/maps/api/js?key=AIzaSyDHjVwbrybvPdtS8_BKSq2Gy4b_e4dOwaA'),false, false);
      wp_register_script( 'custom-functions', get_template_directory_uri() . '/js/functions.min.js', array('jQuery'), '1.0', true );

			wp_enqueue_script('jQuery');
			wp_enqueue_script('map-api');
			wp_enqueue_script('custom-functions');

  }

  add_action( 'wp_enqueue_scripts', 'load_dependencies' );

	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

	// Remove each style one by one
	add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
	function jk_dequeue_styles( $enqueue_styles ) {
		unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
		return $enqueue_styles;
	}

	// Or just remove them all in one line
	add_filter( 'woocommerce_enqueue_styles', '__return_false' );

	/**
	* Breadcrumbs
	*/

	include_once( 'includes/breadcrumb.php' );

	// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php).
	// Used in conjunction with https://gist.github.com/DanielSantoro/1d0dc206e242239624eb71b2636ab148
	add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

	function woocommerce_header_add_to_cart_fragment( $fragments ) {
		global $woocommerce;

		ob_start();
		$cart_has_items = $woocommerce->cart->cart_contents_count > 0 ? 'has-items' : '';

		?>
		<a class="nav-shopping-cart" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
			<i class="icon ion-android-cart"></i><?php echo $woocommerce->cart->get_cart_total(); ?>
			<span class="shopping-cart-item-count <?php print $cart_has_items ?>"><?php echo sprintf(_n('%d <span class="cart-count-text">item</span>', '%d <span class="cart-count-text">items</span>', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span><i class="icon ion-chevron-right"></i>
		</a>
		<?php

		$fragments['a.nav-shopping-cart'] = ob_get_clean();

		return $fragments;

	}

	add_filter('show_admin_bar', '__return_false');

	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');

    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => __('Sidebar Widgets','html5reset' ),
    		'id'   => 'sidebar-widgets',
    		'description'   => __( 'These are widgets for the sidebar.','html5reset' ),
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h3>',
    		'after_title'   => '</h3>'
    	));
    }

    // custom menu support
    add_theme_support( 'menus' );
    if ( function_exists( 'register_nav_menus' ) ) {
        register_nav_menus(
            array(
              'header-menu' => 'Header Menu',
              'footer-menu' => 'Footer Menu',
							'mobile-nav' => 'Mobile Nav'
            )
        );
    }

    add_theme_support( 'post-thumbnails' );

    //add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.

    function wpe_excerptlength_index($length) {
        return 75;
    }
    function wpe_excerptmore($more) {
        return ' ...';
    }

    function wpe_excerpt($length_callback='', $more_callback='') {
        global $post;

    if(function_exists($length_callback)){
        add_filter('excerpt_length', $length_callback);
    }

    if(function_exists($more_callback)){
        add_filter('excerpt_more', $more_callback);
    }

        $output = get_the_excerpt();
        $output = apply_filters('wptexturize', $output);
        $output = apply_filters('convert_chars', $output);
        $output = '<p>'.$output.'</p>';
        echo $output;
    }

    function short_title() {
        $thetitle = get_the_title();
        $getlength = strlen($thetitle);
        $thelength = 45;

        echo substr($thetitle, 0, $thelength);

        if ($getlength > $thelength) echo "...";
    }

    /**
     * Optional: set 'ot_show_pages' filter to false.
     * This will hide the settings & documentation pages.
     */
    add_filter( 'ot_show_pages', '__return_false' );

    /**
     * Required: set 'ot_theme_mode' filter to true.
     */
    add_filter( 'ot_theme_mode', '__return_true' );

    /**
     * Required: include OptionTree.
     */
    include_once( 'libs/option-tree/ot-loader.php' );

    /**
     * Theme Options
     */
    include_once( 'includes/theme-options.php' );


    function filter_textarea_simple_wpautop( $content, $field_id ) {

        if ( $field_id == 'promo_text' ) {
            return true;
        }

        return $content;

    }
    add_filter( 'ot_wpautop', 'filter_textarea_simple_wpautop', 10, 2 );

?>
