<?php get_header();?>

<?php if ( function_exists( 'ot_get_option' ) ) {
	//Main Header Content
	$header_text = ot_get_option( 'heading_text' );
	$sub_heading_text = ot_get_option( 'sub_heading_text' );
	$header_foreground_image = ot_get_option( 'header_foreground_image' );
	$header_background_image = ot_get_option( 'header_background_image' );
	$header_link = get_permalink(ot_get_option( 'header_link' ));
	//Header Column 1
	$header_col_1_heading = ot_get_option( 'home_page_header_col_1_title' );
	$header_col_1_sub_heading = ot_get_option( 'home_page_header_col_1_sub_title' );
	$header_col_1_image = ot_get_option( 'home_page_header_col_1_image' );
	$header_col_1_link = get_permalink(ot_get_option( 'home_page_header_col_1_link' ));
	//Header Column 2
	$header_col_2_heading = ot_get_option( 'home_page_header_col_2_title' );
	$header_col_2_sub_heading = ot_get_option( 'home_page_header_col_2_sub_title' );
	$header_col_2_image = ot_get_option( 'home_page_header_col_2_image' );
	$header_col_2_link = get_permalink(ot_get_option( 'home_page_header_col_2_link' ));
	//Header Column 3
	$header_col_3_heading = ot_get_option( 'home_page_header_col_3_title' );
	$header_col_3_sub_heading = ot_get_option( 'home_page_header_col_3_sub_title' );
	$header_col_3_image = ot_get_option( 'home_page_header_col_3_image' );
	$header_col_3_link = get_permalink(ot_get_option( 'home_page_header_col_3_link' ));
	//Header Column 4
	$header_col_4_heading = ot_get_option( 'home_page_header_col_4_title' );
	$header_col_4_sub_heading = ot_get_option( 'home_page_header_col_4_sub_title' );
	$header_col_4_image = ot_get_option( 'home_page_header_col_4_image' );
	$header_col_4_link = get_permalink(ot_get_option( 'home_page_header_col_4_link' ));
}?>

	<div class="primary-promo" style="background-image: url(<?php print $header_background_image ?>);">
    <div class="container">
			<div class="header-foreground-image" style="background-image: url(<?php print $header_foreground_image ?>);"></div>
    	<div class="promo-text">
    		<h1><?php print $header_text ?></h1>
    		<p><?php print $sub_heading_text ?></p>
    		<a class="primary-button large-button" href="<?php print $header_link ?>">Learn More</a>
    	</div>
    </div>
	</div>

  <div class="secondary-promo">

    <div class="promo-images">
      <div class="container">
        <div class="image-wrapper one-fourth-column">
          <img src="<?php print $header_col_1_image ?>"/>
        </div>

        <div class="image-wrapper one-fourth-column">
          <img src="<?php print $header_col_2_image ?>"/>
        </div>

        <div class="image-wrapper one-fourth-column">
          <img src="<?php print $header_col_3_image ?>"/>
        </div>

        <div class="image-wrapper one-fourth-column last">
          <img src="<?php print $header_col_4_image ?>"/>
        </div>
      </div>

    </div>

    <div class="promo-descriptions">
      <div class="promo-descriptions-container">
        <div class="one-fourth-column superlink">
            <h2><?php print $header_col_1_heading ?></h2>
            <p><?php print $header_col_1_sub_heading ?></p>
            <a href="<?php print $header_col_1_link ?>">Learn More <i class="icons ion-ios-arrow-forward"></i></a>
        </div>
        <div class="one-fourth-column superlink">
            <h2><?php print $header_col_2_heading ?></h2>
            <p><?php print $header_col_2_sub_heading ?></p>
            <a href="<?php print $header_col_2_link ?>">Learn More <i class="icons ion-ios-arrow-forward"></i></a>
        </div>
        <div class="one-fourth-column superlink">
          <h2><?php print $header_col_3_heading ?></h2>
          <p><?php print $header_col_3_sub_heading ?></p>
          <a href="<?php print $header_col_3_link ?>">Learn More <i class="icons ion-ios-arrow-forward"></i></a>
        </div>
        <div class="one-fourth-column superlink last">
          <h2><?php print $header_col_4_heading ?></h2>
          <p><?php print $header_col_4_sub_heading ?></p>
          <a href="<?php print $header_col_4_link ?>">Learn More <i class="icons ion-ios-arrow-forward"></i></a>
        </div>
      </div>
    </div>
  </div>

  <div class="container content">
    <div class="two-third-column" id="main-column">

      <h2>Track Times</h2>

  		<?php query_posts('posts_per_page='.get_option('posts_per_page').'&cat=2&paged='.get_query_var('paged'));

  			if (have_posts()) : while (have_posts()) : the_post();

  				include('includes/post-teaser.php');

  			endwhile;

  				include (TEMPLATEPATH . '/includes/nav.php' );

  			else : echo '<h2>Not Found</h2>';

  				endif;

          ?>
    </div>

   <?php get_sidebar(); ?>

  </div>

<?php get_footer(); ?>
