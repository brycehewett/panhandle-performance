          <footer id="footer">

            <div class="container">
              <div class="one-fourth-column footer-logo-column first">
                <a class="footer-logo" href="<?php echo get_option('home'); ?>/">
                  <img src="<?php bloginfo('template_directory'); ?>/images/gray-logo.png"/>
                </a>
                <span id="siteseal"><script async type="text/javascript" src="https://seal.starfieldtech.com/getSeal?sealID=xjIT0cERDZFAtUMuljXQqnVSBgeTZxwoKpJx5CUETgLiAJ9u0YPJ0DqKtjTK"></script></span>
              </div>

                <?php $defaults = array(
                  'menu'            => 'footer-menu',
                  'container'       => '',
                  'echo'            => true,
                  'fallback_cb'     => 'wp_page_menu',
                  'items_wrap'      => '<ul class="footer-nav three-fourth-column last">%3$s</ul>',
                  'depth'           => 2,);
                wp_nav_menu( $defaults ); ?>

              </div>
              <small class="copyright">&copy; <?php echo date("Y"); echo " "; bloginfo('name'); ?></small>
          </footer>

          <?php wp_footer(); ?>

          <?php
            if ( function_exists( 'ot_get_option' ) ) {
              $google_analytics = ot_get_option( 'google_analytics' );

              if ( ! empty( $google_analytics ) ) {
                echo "<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create', '".$google_analytics."', 'auto');ga('send', 'pageview');</script>";
              }
            }?>

    </div>
  </body>
</html>
