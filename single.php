<?php get_header(); ?>

  <div class="content container">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article class="<?php get_post_class() ?> two-third-column" id="post-<?php the_ID(); ?>">

			<h1><?php the_title(); ?></h1>
				<p class="post-meta"><span><i class="icon ion-android-time"></i> Posted on <time datetime="<?php echo date(DATE_W3C); ?>" pubdate class="updated"><?php the_time('d M, Y') ?></time></span> by <?php the_author_posts_link() ?> in <?php the_category(', ') ?></p>

			<div class="entry-content">
				<div id="teaser-image">
					<?php if ( has_post_thumbnail() ) { ?>
				  	<?php the_post_thumbnail('large'); ?>
					<?php } ?>
				</div>

				<?php the_content(); ?>

				<footer class="postmetadata">
					<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
					<?php the_tags( 'Tags: ', ', ', ''); ?>
				</footer>

				<?php edit_post_link('Edit this entry','',''); ?>

				<div id="respond">

					<?php /* If a user fills out their bio info, it's included here */ ?>
					<div id="post-author">
						<?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_email(), '58' ); /* This avatar is the user's gravatar (http://gravatar.com) based on their administrative email address */  } ?>

						<div id="author-description">
							<p>Written by <?php the_author_posts_link() ?></p>

							<?php $shortlink = wp_get_shortlink(); ?>

							<a id="share_twitter" class="secondary-button medium-button" title="Tweet This" target="_blank" href="http://twitter.com/home?status=<?php echo str_replace("|", "", get_the_title()); echo(' '); echo $shortlink; ?>">Tweet This</a>

							<a id="share_facebook" class="secondary-button medium-button" title="Share This" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" target="_blank">Share This</a>
						</div>
					</div><!--#post-author-->

					<div id="post-nav">
						<div class="prev-posts">
							<?php previous_post_link('%link', '<i class="icons ion-ios-arrow-back"></i> Previous Post') ?>
						</div><!--.older-->
						<div class="next-posts">
								<?php next_post_link('%link', 'Next Post <i class="icons ion-ios-arrow-forward"></i>') ?>
						</div><!--.older-->
					</div>

					<?php comments_template( '', true ); ?>
				</div><!--#respond-->
			</div><!--.entry-content-->

		</article>
	<?php endwhile; endif; ?>
  <?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
