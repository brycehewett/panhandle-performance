// remap jQuery to $

(function($){
$(document).ready(function (){


  $(document).scroll(
    function(){
      if( $(document).scrollTop() > 250 ){
        $('.header').addClass('pinned');
      }
      else{
        $('.header').removeClass('pinned');
      }
    }
  );


  function toggle_menu(){
    $('.menu-toggle, .responsive-menu').toggleClass('active');
    $('.site-wrapper, .header').toggleClass('pushed');
    $('.menu-toggle .icon').toggleClass('ion-close-round');
    $('.menu-toggle .icon').toggleClass('ion-navicon-round');
  }

  $('.menu-toggle').click( function(){
    toggle_menu();
  });

  $(".superlink").click(function(){
    window.location=$(this).find("a").attr("href");
    return false;
  });

  $(".main-nav li").mouseenter(function(){
      $(this).children('.sub-menu').stop(true, true).delay('100').fadeIn('fast');
    });

    $(".main-nav li").mouseleave(function(){
      $(this).children('.sub-menu').stop(true, true).delay('50').fadeOut('fast');
  });

  /*$('#menubutton').toggle(function(){

    $('#site-wrapper').animate({
      right: "210px"
    }, 250);

    $(this).addClass('active');
  },

  function() {
    $('#site-wrapper').animate({
      right: "0"
    }, 250);

    $(this).removeClass('active');

  });*/
});
})(jQuery);
