<?php get_header(); ?>
<?php
	$breadcrumb_args = array(
    'wrap_before' => '<div class="crumbs">',
    'wrap_after' => '</div>',
		'delimiter' => ' / '
	);
?>

  <div class="breadcrumbs">
    <div class="container">
      <?php woocommerce_breadcrumb( $breadcrumb_args ); ?>
    </div>
  </div>
  <div class="container content">
    <?php woocommerce_content(); ?>
  </div>
</div>

<?php get_footer(); ?>
