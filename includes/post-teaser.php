<article>
  <p class="post-meta"><span><i class="icon ion-android-time"></i> Posted on <time datetime="<?php echo date(DATE_W3C); ?>" pubdate class="updated"><?php the_time('d M, Y') ?></time></span> by <?php the_author_posts_link() ?> in <?php the_category(', ') ?>
  </p>

  <div class="post-teaser-wrapper">
      <?php if ( has_post_thumbnail() ) { ?>
        <div class="teaser-thumbnail">
          <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php the_post_thumbnail('large'); ?></a>
        </div>
      <?php } ?>

      <h3><a href="<?php the_permalink() ?>"><?php short_title(); ?></a></h3>

      <?php the_excerpt(); ?>
    <div class="teaser-bottom">
      <a class="comment-link" href="<?php the_permalink(); ?>#respond">View Comments</a>
      <a class="article-link" href="<?php the_permalink(); ?>">Read More</a>
    </div>

  </div>
</article>
