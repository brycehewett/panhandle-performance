<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array.
   */
  $saved_settings = get_option( 'option_tree_settings', array() );

  /**
   * Custom settings array that will eventually be
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array(
    'contextual_help' => array(
      'sidebar'       => ''
    ),
    'sections'        => array(
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
      array(
        'id'          => 'social',
        'title'       => 'Social'
      ),
      array(
        'id'          => 'home_page_header',
        'title'       => 'Home Page Header'
      ),
      array(
        'id'          => 'home_page_header_column_1',
        'title'       => 'Home Page Header Column 1'
      ),
      array(
        'id'          => 'home_page_header_column_2',
        'title'       => 'Home Page Header Column 2'
      ),
      array(
        'id'          => 'home_page_header_column_3',
        'title'       => 'Home Page Header Column 3'
      ),
      array(
        'id'          => 'home_page_header_column_4',
        'title'       => 'Home Page Header Column 4'
      )
    ),
    'settings'        => array(
      array(
        'id'          => 'google_analytics',
        'label'       => 'Google Analytics',
        'desc'        => 'Enter your Google Analytics UA code here.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'favicon',
        'label'       => 'Favicon',
        'desc'        => 'Favicon size should be 16x16px. Make sure to use an image with .ico extension.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'apple_touch_icon',
        'label'       => 'Apple Touch Icon',
        'desc'        => '57x57 for older iPhones, 72x72 for iPads, 114x114 for the iPhone4 retina display. Just go ahead and use the biggest one. Transparency is not recommended',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'twitter_url',
        'label'       => 'Twitter URL',
        'desc'        => 'Enter your Twitter url.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'facebook_app_id',
        'label'       => 'Facebook App ID',
        'desc'        => 'Enter your facebook app ID here.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'facebook_url',
        'label'       => 'Facebook URL',
        'desc'        => 'Enter your Facebook url.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'default_facebook_logo',
        'label'       => 'Default Facebook Logo',
        'desc'        => 'Upload the logo that will be used in case there is no image on the page for facebook to use.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'social',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'heading_text',
        'label'       => 'Heading Text',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page_header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'sub_heading_text',
        'label'       => 'Sub-Heading Text',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page_header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'header_foreground_image',
        'label'       => 'Header Foreground Image',
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'home_page_header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'header_background_image',
        'label'       => 'Header Backgound Image',
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'home_page_header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'header_link',
        'label'       => 'Header Link',
        'desc'        => '',
        'std'         => '',
        'type'        => 'page-select',
        'section'     => 'home_page_header',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_1_title',
        'label'       => 'Heading',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page_header_column_1',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_1_sub_title',
        'label'       => 'Sub Heading',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page_header_column_1',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_1_image',
        'label'       => 'Image',
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'home_page_header_column_1',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_1_link',
        'label'       => 'Link',
        'desc'        => '',
        'std'         => '',
        'type'        => 'page-select',
        'section'     => 'home_page_header_column_1',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),

      array(
        'id'          => 'home_page_header_col_2_title',
        'label'       => 'Heading',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page_header_column_2',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_2_sub_title',
        'label'       => 'Sub Heading',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page_header_column_2',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_2_image',
        'label'       => 'Image',
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'home_page_header_column_2',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_2_link',
        'label'       => 'Link',
        'desc'        => '',
        'std'         => '',
        'type'        => 'page-select',
        'section'     => 'home_page_header_column_2',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),

      array(
        'id'          => 'home_page_header_col_3_title',
        'label'       => 'Heading',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page_header_column_3',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_3_sub_title',
        'label'       => 'Sub Heading',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page_header_column_3',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_3_image',
        'label'       => 'Image',
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'home_page_header_column_3',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_3_link',
        'label'       => 'Link',
        'desc'        => '',
        'std'         => '',
        'type'        => 'page-select',
        'section'     => 'home_page_header_column_3',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),

      array(
        'id'          => 'home_page_header_col_4_title',
        'label'       => 'Heading',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page_header_column_4',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_4_sub_title',
        'label'       => 'Sub Heading',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page_header_column_4',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_4_image',
        'label'       => 'Image',
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'home_page_header_column_4',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'home_page_header_col_4_link',
        'label'       => 'Link',
        'desc'        => '',
        'std'         => '',
        'type'        => 'page-select',
        'section'     => 'home_page_header_column_4',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
    )
  );

  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );

  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings );
  }

}
