<?php $defaults = array(
  'menu'            => 'mobile-nav',
  'container'       => '',
  'echo'            => true,
  'fallback_cb'     => 'wp_page_menu',
  'items_wrap'      => '<nav class="responsive-menu"><ul>%3$s</ul></nav>',
  'depth'           => 2,);
wp_nav_menu( $defaults ); ?>
</nav>
