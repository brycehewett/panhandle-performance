<header>
  <div class="header">
    <div class="top-nav">
      <a class="logo" href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a>

        <?php $defaults = array(
          'menu'            => 'header-menu',
          'container'       => '',
          'echo'            => true,
          'fallback_cb'     => 'wp_page_menu',
          'items_wrap'      => '<ul class="main-nav">%3$s</ul>',
          'depth'           => 2,);
        wp_nav_menu( $defaults ); ?>

        <div class="secondary-top-nav">
          <?php global $woocommerce; ?>
          <a class="nav-shopping-cart" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
      			<i class="icon ion-android-cart"></i><?php echo $woocommerce->cart->get_cart_total(); ?>
      			<span class="shopping-cart-item-count <?php print $cart_has_items ?>"><?php echo sprintf(_n('%d <span class="cart-count-text">item</span>', '%d <span class="cart-count-text">items</span>', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span><i class="icon ion-chevron-right"></i>
      		</a>
        </div>
        <div class="menu-toggle"><i class="icon ion-navicon-round"></i></div>
    </div>
  </div>
</header>
